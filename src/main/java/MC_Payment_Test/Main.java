/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MC_Payment_Test;

import java.util.Scanner;

/**
 *
 * @author axelyudhiputra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        int avail = 1;
        
        while(avail == 1){
            System.out.print("Input Soal (1/2/3): "); int input_soal = scan.nextInt();
            
            switch(input_soal){
                case 1:
                    Soal1();
                    break;
                case 2:
                    Soal2();
                    break;
                case 3:
                    Soal3();
                    break;
                default:
                    avail = 0;
                    EndFunc();
                    break;
            }
        }
        
        
    }
    
    public static void Soal1(){
        int x = 0;
        System.out.println("======================");
        System.out.println("========SOAL-1========");
        System.out.print("Input Array Length : "); int arraylength = scan.nextInt();
        int nums[] = new int[arraylength];
        for (int i = 0; i < arraylength; i++) {
            System.out.print("Input angka ke-" + (i+1) + ": "); int temp = scan.nextInt();
            nums[i] = temp;
        }
        System.out.print("output : ");
        for (int i = 0; i < arraylength; i++){
            int total_check = 0;
            for (int j = 0; j < arraylength; j++) {
                if(nums[i] - nums[j] >= 0){
                    total_check += 1;
                }
            }
            if(total_check == arraylength){
                if(x > 0){
                    System.out.print(", ");
                }
                System.out.print(nums[i]);
                x += 1;
            }
        }
        System.out.println("");
        System.out.println("======================");
    }
    
    public static void Soal2(){
        int z = 0;
        System.out.println("======================");
        System.out.println("========SOAL-2========");
        System.out.print("Input Array Length : "); int arraylength = scan.nextInt();
        int nums[] = new int[arraylength];
        for (int i = 0; i < arraylength; i++) {
            System.out.print("Input angka ke-" + (i+1) + ": "); int temp = scan.nextInt();
            nums[i] = temp;
        }
        System.out.println("======================");
        System.out.print("Input X : "); int x = scan.nextInt();
        System.out.print("output : ");
        for (int i = 0; i < arraylength; i++){
            int total_check = 0;
            for (int j = 0; j < arraylength; j++) {
                if(nums[i] / nums[j] == x && nums[i] % nums[j] == 0){
                    total_check += 1;
                    if(z > 0){
                        System.out.print(", ");
                    }
                    System.out.print(nums[i]);
                    z += 1;
                    j += arraylength;
                }
            }
        }
        System.out.println("");
        System.out.println("======================");
        
    }
    
    public static void Soal3(){
        int z = 0;
        System.out.println("======================");
        System.out.println("========SOAL-3========");
        System.out.print("Input word/words : "); String word = scan.next();
        word += scan.nextLine();
        System.out.print("Input X : "); int x = scan.nextInt();
        String[] words = word.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[^\\w]", "");
        }
        System.out.print("Output : ");
        for (int i = 0; i < words.length; i++){
//            System.out.print(words[i].length() + " ");
            if(words[i].length() == x){
                if(z > 0){
                    System.out.print(", ");
                }
                System.out.print(words[i]);
                z += 1;
            }
        }
        System.out.println("");
        System.out.println("======================");
    }
    
    public static void EndFunc(){
        System.out.println("///////THANKYOU///////");
    }
    
}
